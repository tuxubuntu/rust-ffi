
use std::ffi::CString;

pub struct Test {
	pub title: CString,
	pub args: Box<[i32]>,
	pub a: i32,
	pub b: i32,
	pub res: bool
}

#[no_mangle]
pub extern fn eq(a: i32, b: i32) -> Test {
	println!("{:} & {:}", a, b);
	Test {
		title: CString::new("lalka").unwrap(),
		args: Box::new([a, b]),
		a,
		b,
		res: a == b
	}
}

#[no_mangle]
pub extern fn res(res: &Test) -> bool {
	res.res
}

#[cfg(test)]
mod tests {
	use *;
	#[test]
	fn eq_test() {
		assert_eq!(eq(2, 3), false);
		assert_eq!(eq(2, 2), true);
	}
}
