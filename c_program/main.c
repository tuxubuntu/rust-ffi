#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

typedef struct {
	char* str;
	uint length;
} String;

typedef struct {
	int* value;
	uint length;
} IntList;

typedef struct {
	String title;
	IntList args;
	int a;
	int b;
	char res;
} Test;

int main() {
	typedef Test (*eq_fn)(int, int);
	typedef char (*res_fn)(Test*);
	void* dlh = dlopen("../rust_lib/target/release/libtest.so", RTLD_LAZY);
	if (dlh == NULL) {
		fprintf(stderr, "Error: %s\n", dlerror());
		return 1;
	}
	eq_fn eq = dlsym(dlh, "eq");
	res_fn res = dlsym(dlh, "res");
	int x = 55;
	int y = -22;
	Test r = eq(x, y);
	char s = res(&r);
	printf("%s: %d == %d => %d\n", r.title.str, r.a, r.b, s);
	return 0;
}
